# Person class

from abc import ABC
from datetime import datetime

class Person(ABC):
    
    def getFullName(self):
        pass

    def addRequest(self):
        pass

    def checkRequest(self):
        pass

    def addUser(self):
        pass

# Employee class
class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        # super().__init__(firstName, lastName, email, department)
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._request = []

    # private and getters/setters
    def getFirstName(self):
        return self._firstName

    def setFirstName(self, firstName):
        self._firstName = firstName
        
    def getLastName(self):
        return self._lastName
    
    def setLastName(self, lastName):
        self._lastName = lastName
        
    def getEmail(self):
        return self._email
    
    def setEmail(self, email):
        self._email = email
        
    def getDepartment(self):
        return self._department
    
    def setDepartment(self, department):
        self._department = department

    # methods
    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    def addRequest(self, request):
        pass
        

    def checkRequest(self):
        return "Not Available for regular employees"
        pass

    def addUser(self, user):
        return "Not Available for regular employees"
        pass

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

# TeamLead class
class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        # super().__init__(firstName, lastName, email, department)
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []

    
    def getFirstName(self):
        return self._firstName
    
    def setFirstName(self, firstName):
        self._firstName = firstName
        
    def getLastName(self):
        return self._lastName
    
    def setLastName(self, lastName):
        self._lastName = lastName
        
    def getEmail(self):
        return self._email
    
    def setEmail(self, email):
        self._email = email
        
    def getDepartment(self):
        return self._department
    
    def setDepartment(self, department):
        self._department = department
        
    def getMembers(self):
        return self._members

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    def addRequest(self, request):
        pass

    def checkRequest(self, request):
        return ""

    def addUser(self, person):
        if isinstance(person, Employee):
            self._members.append(person)

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def addMember(self, employee):
        self._members.append(employee)

# Admin class
class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._users = []

    def getFirstName(self):
        return self._firstName
    
    def setFirstName(self, firstName):
        self._firstName = firstName
        
    def getLastName(self):
        return self._lastName
    
    def setLastName(self, lastName):
        self._lastName = lastName
        
    def getEmail(self):
        return self._email
    
    def setEmail(self, email):
        self._email = email
        
    def getDepartment(self):
        return self._department
    
    def setDepartment(self, department):
        self._department = department

    def getFullName(self):
        return f"{self._firstName} {self._lastName}"

    def addRequest(self, request):
        pass

    def checkRequest(self):
        return ""

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def addUser(self, person):
        self._users.append(person)

class Request(Person):
    def __init__(self, name, requester, dateRequested):
        self._name = name
        self._requester = requester
        self._dateRequested = dateRequested
        self._status = "Open"

    def updateRequest(self, newStatus):
        self._status = newStatus

    def closeRequest(self):
        self._status = "Closed"
        return f"Request {self._name} has been {self._status}"

    def cancelRequest(self):
        self._status = "Cancelled"

    def getName(self):
        return self._name

    def setName(self, name):
        self._name = name

    def getRequester(self):
        return self._requester

    def setRequester(self, requester):
        self._requester = requester

    def getDateRequested(self):
        return self._dateRequested

    def setDateRequested(self, dateRequested):
        self._dateRequested = dateRequested

    def getStatus(self):
        return self._status

    def set_status(self, status):
        self._status = status

# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
# assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.getMembers():
    print(indiv_emp.getFullName())

# assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())



